﻿using System.Diagnostics;
using Microsoft.AspNetCore.Mvc;
using InfoTrackSearch.Models;
using InfoTrackSearch.Helper;
using Microsoft.Extensions.Configuration;
using System;
using Microsoft.AspNetCore.Authorization;

namespace InfoTrackSearch.Controllers
{
    public class HomeController : Controller
    {
        string privateSecretKey = String.Empty;

        public HomeController(IConfiguration configuration)
        {
            privateSecretKey = configuration["PrivateSecretKey"];
        }

        [AllowAnonymous]
        public IActionResult Index()
        {
            return View();
        }

        [AllowAnonymous]
        public IActionResult Signin()
        {
            string token = new Authorize().GetToken(privateSecretKey);
            return Ok(token);
        }

        [ResponseCache(Duration = 0, Location = ResponseCacheLocation.None, NoStore = true)]
        public IActionResult Error()
        {
            return View(new ErrorViewModel { RequestId = Activity.Current?.Id ?? HttpContext.TraceIdentifier });
        }
    }
}
