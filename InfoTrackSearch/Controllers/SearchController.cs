﻿using System.Threading.Tasks;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using SearchBL;

namespace InfoTrackSearch.Controllers
{
    public class SearchController : Controller
    {
        ISearchServiceProvider _SearchServiceProvider;
        public SearchController(ISearchServiceProvider searchServiceProvider)
        {
            _SearchServiceProvider = searchServiceProvider;
        }

        /// <summary>
        /// GET: Search the text on the Google and then use default GoogleResponseHandler
        /// to analyse the search result.
        /// </summary>
        /// <param name="textToSearch">Text to search on Google</param>
        /// <param name="urlToSearch">Url to search in the Google search response</param>
        /// <returns>Search analysis</returns>
        [Authorize(Roles = "CEO")]
        [HttpGet]
        public IActionResult SearchOnGoogle(string textToSearch, string urlToSearch)
        {
            if (string.IsNullOrEmpty(textToSearch) || string.IsNullOrEmpty(urlToSearch))
            {
                // Error 400: User Error
                return BadRequest();
            }

            //Any exception here is handled at central location and returned as 500.
            Task<string> task = _SearchServiceProvider.GoogleSearchService.SearchAsync(new SearchParams()
            {
                TextToSearch = textToSearch,
                WebLinkToSearch = urlToSearch
            }, new SearchBL.ResponseHandler.GoogleResponseHandler());

            // Success 200: OK
            return Ok(task.Result);
        }

        /// <summary>
        /// GET: Search the text on the Bing and then use default BingResponseHandler
        /// to analyse the search result.
        /// </summary>
        /// <param name="textToSearch">Text to search on Bing</param>
        /// <param name="urlToSearch">Url to search in the Bing search response</param>
        /// <returns>Search analysis</returns>
        [Authorize(Roles = "CEO")]
        [HttpGet]
        public IActionResult SearchOnBing(string textToSearch, string urlToSearch)
        {
            if (string.IsNullOrEmpty(textToSearch) || string.IsNullOrEmpty(urlToSearch))
            {
                // Error 400: User Error
                return BadRequest();
            }

            //Any exception here is handled at central location and returned as 500.
            Task<string> task = _SearchServiceProvider.BingSearchService.SearchAsync(new SearchParams()
            {
                TextToSearch = textToSearch,
                WebLinkToSearch = urlToSearch
            }, new SearchBL.ResponseHandler.BingResponseHandler());

            // Success 200: OK
            return Ok(task.Result);
        }
    }
}