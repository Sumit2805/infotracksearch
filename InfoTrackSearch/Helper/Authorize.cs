﻿using Microsoft.IdentityModel.Tokens;
using System;
using System.Collections.Generic;
using System.IdentityModel.Tokens.Jwt;
using System.Linq;
using System.Security.Claims;
using System.Text;
using System.Threading.Tasks;

namespace InfoTrackSearch.Helper
{
    /// <summary>
    /// Class responsible for authorizing the user
    /// </summary>
    public class Authorize
    {
        /// <summary>
        /// Get the auth token and claim
        /// </summary>
        /// <param name="privateSecretKey">Encrypted private key</param>
        /// <returns>Encrypted token string</returns>
        public string GetToken(string privateSecretKey)
        {
            string role = "CEO";
            string name = "Mr. CEO";
            var claims = new List<Claim>
            {
                    new Claim(ClaimTypes.Name, name),
                    new Claim(ClaimTypes.Role, role)
            };

            var key = new SymmetricSecurityKey(Encoding.UTF8.GetBytes(privateSecretKey));
            var creds = new SigningCredentials(key, SecurityAlgorithms.HmacSha256);

            var token = new JwtSecurityToken(
                issuer: "infotrack.com.au",
                audience: "infotrack.com.au",
                claims: claims.ToArray(),
                expires: DateTime.Now.AddMinutes(15),
                signingCredentials: creds);

            return new JwtSecurityTokenHandler().WriteToken(token);
        }
    }
}
