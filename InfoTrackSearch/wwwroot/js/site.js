﻿// Please see documentation at https://docs.microsoft.com/aspnet/core/client-side/bundling-and-minification
// for details on configuring this project to bundle and minify static web assets.

// Write your JavaScript code.

/*
 * SEARCH OPERATION Operation
 */

function doSearch() {
    let textToSearch = $("#textToSearch").val();
    let urlToSearch = $("#urlToSearch").val();
    let outputP = $("#searchResult");

    if ((textToSearch !== null && textToSearch !== "") &&
        (urlToSearch !== null && urlToSearch !== ""))
    {
        saveToCookies(textToSearch, urlToSearch);

        let action = getAction();

        let searchResult = "'" + textToSearch + "' " + action;
        outputP.html(searchResult);
        outputP.css("color", "black");

        let qTextToSearch = "?textToSearch=" + textToSearch;
        let qUrlToSearch = "&urlToSearch=" + urlToSearch;

        let url = "search/" + action + qTextToSearch + qUrlToSearch;
        let token = $("#token").html();
        $.ajax({
                'url': url,
                'headers': { 'Authorization': token }
            })
            .done(function (data, status)
                  {
                searchResult += "</br>" + data;
                searchResult += "</br>" + "Search completed.";
                outputP.html(searchResult);
                 })
            .fail(function (e)
                {
                    searchResult += "</br>";
                    if (e.status === 400) {
                        searchResult += "Invalid input.";
                    }
                    else if (e.status === 401) {
                        searchResult = "Unauthorized to perform search. Please Sign-in.";
                    }
                    else if (e.status === 500) {
                        searchResult += "Internal server error.";
                    }
                    outputP.html(searchResult);
                    outputP.css("color", "red");
                });
    }
    else {
        showErrorMessage();
    }
}

function getAction() {
    let google = $("#customRadioInline1:checked");
    let bing = $("#customRadioInline2:checked");

    if (google.is(":checked")) {
        return google.val();
    }
    else if (bing.is(":checked")) {
        return bing.val();
    }
}

function showErrorMessage() {
    let textToSearch = $("#textToSearch").val();
    if (textToSearch === null || textToSearch === "") {
        $("#textToSearchError").fadeIn(500);
        $("#textToSearchError").fadeOut(2000);
    }

    let urlToSearch = $("#urlToSearch").val();
    if (urlToSearch === null || urlToSearch === "") {
        $("#urlToSearchError").fadeIn(500);
        $("#urlToSearchError").fadeOut(2000);
    }
}

function getHeaders() {
    let token = $("#token").html();
    const headers = new Headers();
    headers.append('Authorization', 'Bearer ' + token);
    return headers;
}

/*
 * Signin
 */
function btnSignInClicked() {
    let outputP = $("#searchResult");
    let url = "home/signin";
    $.ajax({
        'url': url
    })
        .done(function (data, status) {
            $("#token").html('Bearer ' + data);
            outputP.html("Successfully signed-in.");
            outputP.css("color", "green");
        })
        .fail(function (e) {
            $("#token").html('');
            if (e.status === 500) {
                outputP.html("Sign-in failed. Server internal error.");
            }
            else {
                outputP.html("Sign-in failed.");
            }
            outputP.css("color", "red");
        });
}


/*
 * DOCUMENT READY
 */

$(document).ready(function () {
    $("#textToSearchError").hide();
    $("#urlToSearchError").hide();

    getFromCookie();
});

/*
 * BUTTON CLICKS HANDLING
 */

function btnSearchClicked() {
    doSearch();
}

function btnDefaultClicked() {
    $("#textToSearch").val("Online Title Search");
    $("#urlToSearch").val("www.infotrack.com.au");
}

function btnClearAllClicked() {
    $("#textToSearch").val('');
    $("#urlToSearch").val('');
    $("#searchResult").html('');
}

function btnRestoreFromCookiesClicked() {
    getFromCookie();
}

/*
 * COOKIE HANDLING
 */

function saveToCookies(text1, text2) {
    if (requestToRemeberTheSearch()) {
        document.cookie = 'T1=' + text1;
        document.cookie = 'T2=' + text2;
    }
}

function requestToRemeberTheSearch() {
    let remeberCheckBox = $("#customControlCheck1:checked");

    if (remeberCheckBox.is(":checked")) {
        return true;
    }
    return false;
}

function getFromCookie() {
    $("#textToSearch").val(getCookie("T1"));
    $("#urlToSearch").val(getCookie("T2"));
}

function getCookie(cname) {
    var name = cname + "=";
    var decodedCookie = decodeURIComponent(document.cookie);
    var ca = decodedCookie.split(';');
    for (var i = 0; i < ca.length; i++) {
        var c = ca[i];
        while (c.charAt(0) === ' ') {
            c = c.substring(1);
        }
        if (c.indexOf(name) === 0) {
            return c.substring(name.length, c.length);
        }
    }
    return "";
}