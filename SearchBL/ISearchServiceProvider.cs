﻿namespace SearchBL
{
    public interface ISearchServiceProvider
    {
        ISearchRequest GoogleSearchService { get; }

        ISearchRequest BingSearchService { get; }
    }
}
