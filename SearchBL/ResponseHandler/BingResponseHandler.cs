﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Net;
using System.Text;

namespace SearchBL.ResponseHandler
{
    /// <summary>
    /// Response handler for Google response
    /// Search through bing response html to find Search links
    /// </summary>
    public class BingResponseHandler : ISearchResponseHandler
    {
        private const string TagBegin = "<li class=\"b_algo\">";
        private const string TagEnd = "</li>";

        private const string HtmlNewLine = "</br>";

        /// <summary>
        /// Analyse the search result
        /// </summary>
        /// <param name="searchParams">Search parameters</param>
        /// <param name="webResponse">Web response</param>
        /// <returns>Analysis result</returns>
        public string HandleSearchResult(SearchParams searchParams, WebResponse webResponse)
        {
            StringBuilder sbResult = new StringBuilder();

            using (Stream receiveStream = webResponse.GetResponseStream())
            {
                using (StreamReader stremaReader = new StreamReader(receiveStream, Encoding.UTF8))
                {
                    string htmlResponse = stremaReader.ReadToEnd();

                    ContentLookup _lookup = new ContentLookup(htmlResponse, searchParams.WebLinkToSearch);

                    sbResult.AppendLine("Link Indices:");
                    sbResult.Append(HtmlNewLine);
                    sbResult.AppendLine(_lookup.GetLinkIndexesBetween(TagBegin, TagEnd));
                    sbResult.Append(HtmlNewLine);
                }
            }
            return sbResult.ToString();
        }
    }
}
