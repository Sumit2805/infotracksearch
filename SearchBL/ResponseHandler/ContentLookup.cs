﻿using System.Text;

namespace SearchBL
{
    class ContentLookup
    {
        string _content;
        string _lookupText;

        public ContentLookup(string content, string lookupText)
        {
            _content = content;
            _lookupText = lookupText;
        }

        public string GetLinkIndexesBetween(string startTag, string endTag)
        {
            StringBuilder sbReturn = new StringBuilder();
            int lastLinkIndex = 0;
            bool continueLoop = true;
            bool lookupTextFound = false;
            int linkIndex = 1;

            do
            {
                int indexLinkStart = _content.IndexOf(startTag, lastLinkIndex);
                if (indexLinkStart < 0)
                {
                    continueLoop = false;
                }
                else
                {
                    int indexLinkEnd = _content.IndexOf(endTag, indexLinkStart);

                    var link = _content.Substring(indexLinkStart, indexLinkEnd - indexLinkStart);
                    if (link.Contains(_lookupText))
                    {
                        lookupTextFound = true;
                        sbReturn.AppendLine(linkIndex.ToString());
                    }
                    lastLinkIndex = indexLinkEnd;
                    linkIndex++;
                }
            } while (continueLoop);

            if (!lookupTextFound)
            {
                sbReturn.Append("0");
            }

            return sbReturn.ToString();
        }

    }
}
