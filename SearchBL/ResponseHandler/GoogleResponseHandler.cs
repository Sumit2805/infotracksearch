﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Net;
using System.Text;

namespace SearchBL.ResponseHandler
{
    /// <summary>
    /// Response handler for Google response
    /// Search through google response html to find Ad and Search links
    /// </summary>
    public class GoogleResponseHandler : ISearchResponseHandler
    {
        private const string H3TagBegin = "<h3 class=\"r\">";
        private const string H3TagEnd = "</h3>";

        private const string AdTagBegin = "<cite class=\"UdQCqe\">";
        private const string AdTagEnd = "</cite>";

        private const string HtmlNewLine = "</br>";

        /// <summary>
        /// Analyse the search result
        /// </summary>
        /// <param name="searchParams">Search parameters</param>
        /// <param name="webResponse">Web response</param>
        /// <returns>Analysis result</returns>
        public string HandleSearchResult(SearchParams searchParams, WebResponse webResponse)
        {
            StringBuilder sbResult = new StringBuilder();

            using (Stream receiveStream = webResponse.GetResponseStream())
            {
                using (StreamReader stremaReader = new StreamReader(receiveStream, Encoding.UTF8))
                {
                    string htmlResponse = stremaReader.ReadToEnd();

                    ContentLookup _lookup = new ContentLookup(htmlResponse, searchParams.WebLinkToSearch);

                    sbResult.Append("Link Indices in Ads:");
                    sbResult.Append(HtmlNewLine);
                    sbResult.Append(_lookup.GetLinkIndexesBetween(AdTagBegin, AdTagEnd));
                    sbResult.Append(HtmlNewLine);

                    sbResult.AppendLine("Link Indices:");
                    sbResult.Append(HtmlNewLine);
                    sbResult.AppendLine(_lookup.GetLinkIndexesBetween(H3TagBegin, H3TagEnd));
                    sbResult.Append(HtmlNewLine);
                }
            }
            return sbResult.ToString();
        }
    }
}
