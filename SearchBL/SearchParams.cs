﻿namespace SearchBL
{
    /// <summary>
    /// Search parameters
    /// </summary>
    public class SearchParams
    {
        /// <summary>
        /// Text to be searched
        /// </summary>
        public string TextToSearch { get; set; } = string.Empty;

        /// <summary>
        /// Number of records to fetch
        /// </summary>
        public int ResultCount { get; set; } = 100;

        /// <summary>
        /// WebLink/Url to be searched in the response
        /// </summary>
        public string WebLinkToSearch { get; set; } = "www.infotrack.com.au";
    }
}
