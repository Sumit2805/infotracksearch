﻿using System;
using System.Net;
using System.Threading.Tasks;
using SearchBL.SearchUrl;

namespace SearchBL
{
    internal abstract class AbstractSearchService : ISearchRequest
    {
        /// <summary>
        /// Search request Url
        /// </summary>
        protected abstract ISearchUrl SearchUrl { get; }

        /// <summary>
        /// Send the http search webrequest
        /// </summary>
        /// <param name="queryParams">Search params</param>
        /// <returns>Task with search result analysis</returns>
        public Task<string> SearchAsync(SearchParams queryParams, ISearchResponseHandler responseHandler)
        {
            Task<string> task = new Task<string>(() => {
                if (queryParams == null) throw new ArgumentNullException("queryParams");
                if (responseHandler == null) throw new ArgumentNullException("responseHandler");

                var url = SearchUrl.BuildUrl(queryParams);
                HttpWebRequest searchRequest = (HttpWebRequest)WebRequest.Create(url);

                WebResponse webResponse = searchRequest.GetResponse();
                return responseHandler.HandleSearchResult(queryParams, webResponse);
            });
            task.Start();
            return task;
        }
    }
}
