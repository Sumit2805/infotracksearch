﻿using SearchBL.SearchUrl;

namespace SearchBL
{
    class BingSearchService : AbstractSearchService
    {
        protected override ISearchUrl SearchUrl => new BingSearchUrl();
    }
}
