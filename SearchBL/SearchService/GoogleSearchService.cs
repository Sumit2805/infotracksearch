﻿using SearchBL.SearchUrl;

namespace SearchBL
{
    class GoogleSearchService : AbstractSearchService
    {
        protected override ISearchUrl SearchUrl => new GoogleSearchUrl();
    }
}
