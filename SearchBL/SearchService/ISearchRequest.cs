﻿using System.Threading.Tasks;

namespace SearchBL
{
    public interface ISearchRequest
    {
        /// <summary>
        /// Handle the search request
        /// </summary>
        /// <param name="queryParams">Search query params</param>
        /// <returns></returns>
        Task<string> SearchAsync(SearchParams queryParams, ISearchResponseHandler responseHandler);
    }
}
