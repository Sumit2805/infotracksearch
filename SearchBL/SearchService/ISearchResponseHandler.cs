﻿using System.Net;

namespace SearchBL
{
    public interface ISearchResponseHandler
    {
        /// <summary>
        /// Analyze the search result/response
        /// </summary>
        /// <param name="searchParams">Search parameters</param>
        /// <param name="webResponse">Web response</param>
        /// <returns>Report search response</returns>
        string HandleSearchResult(SearchParams searchParams, WebResponse webResponse);
    }
}
