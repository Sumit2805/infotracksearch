﻿namespace SearchBL
{
    /// <summary>
    /// Provide access to search services
    /// </summary>
    public class SearchServiceProvider : ISearchServiceProvider
    {
        public ISearchRequest GoogleSearchService => new GoogleSearchService();

        public ISearchRequest BingSearchService => new BingSearchService();
    }
}
