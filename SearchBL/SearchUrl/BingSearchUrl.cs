﻿using System;

namespace SearchBL.SearchUrl
{
    /// <summary>
    /// Bing search url
    /// </summary>
    class BingSearchUrl : ISearchUrl
    {
        private const string queryString = "https://www.bing.com/search?q={0}&First={1}";

        string ISearchUrl.BuildUrl(SearchParams searchParams)
        {
            if (searchParams == null) throw new ArgumentNullException("searchParams");
            if (searchParams.TextToSearch == null)
            {
                searchParams.TextToSearch = String.Empty;
            }

            string textSearchEnhanced = searchParams.TextToSearch.Replace(' ', '+');
            return String.Format(queryString, textSearchEnhanced, 1);
        }
    }
}
