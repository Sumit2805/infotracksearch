﻿using System;

namespace SearchBL.SearchUrl
{
    /// <summary>
    /// Google search url
    /// </summary>
    class GoogleSearchUrl : ISearchUrl
    {
        private const string queryString = "https://www.google.com/search?q={0}&num={1}";

        string ISearchUrl.BuildUrl(SearchParams searchParams)
        {
            if (searchParams == null) throw new ArgumentNullException("searchParams");
            if (searchParams.TextToSearch == null)
            {
                searchParams.TextToSearch = String.Empty;
            }

            string textSearchEnhanced = searchParams.TextToSearch.Replace(' ', '+');
            return String.Format(queryString, textSearchEnhanced, searchParams.ResultCount);
        }
    }
}
