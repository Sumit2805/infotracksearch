﻿namespace SearchBL.SearchUrl
{
    internal interface ISearchUrl
    {
        /// <summary>
        /// Provide the search query string, placing the query params
        /// </summary>
        /// <param name="queryParams">Query param object</param>
        string BuildUrl(SearchParams queryParams);
    }
}
